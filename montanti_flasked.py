import pickle

from flask import Flask, request
from flask_json import FlaskJSON, JsonError, as_json

import preprocessing

app = Flask(__name__)
# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={'errors': [
        {'code': 'elg.request.invalid', 'text': 'Invalid request message'}
    ]})


@app.route('/process', methods=['POST'])
@as_json
def process_request():
    """Main request processing logic - accepts a JSON request and returns a JSON response."""
    data = request.get_json()
    # sanity checks on the request message
    if (data.get('type') != 'text') or ('content' not in data):
        invalid_request_error(None)

    content = data['content']
    result = do_nlp(content)
    return dict(response={'type': 'annotations', 'annotations': result})


@app.before_first_request
def load_models():
    # load spacy sentence segmenter and tokenizer
    import spacy
    global snlp
    snlp = spacy.load('it_core_news_sm', disable=['ner'])
    # load classifier
    global model
    with open('models/model_log_reg_tfidf.pk', "rb") as fin:
        model = pickle.load(fin)
    global vectorizer
    vectorizer = load_tfidf_vectorizer()


def load_tfidf_vectorizer():
    with open('models/tfidf_vectorizer_italian.pk', "rb") as fin:
        data = pickle.load(fin)
    return data


def do_nlp(content):
    sentence_anns = []
    doc = snlp(content)
    test = []
    for sent in doc.sents:
        sent_preprocessed = preprocessing.pre_process(sent.text.lower(), False)
        test.append(sent_preprocessed)
    test_x = vectorizer.transform(test).toarray()
    test_y = model.predict(test_x)
    for sent, label in zip(doc.sents, test_y):
        sentence_anns.append({'start': sent.start_char, 'end': sent.end_char, 'features': {'label':int(label)}})
    return {'Sentence': sentence_anns}


if __name__ == '__main__':
    app.run()
